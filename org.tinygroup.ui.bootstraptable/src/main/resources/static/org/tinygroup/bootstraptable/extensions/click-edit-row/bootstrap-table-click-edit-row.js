(function ($) {
    'use strict';

    $.extend($.fn.bootstrapTable.defaults, {
        clickEdit: false
    });

    var clikcToEdit=function(evt, tarNode,rowData) {
        var txt = [], table = evt,_table=evt.$el,
            submit = '<button type="button" class="btn btn-primary editable-submit"><i class="glyphicon glyphicon-ok"></i></button>',
            cancel = '<button type="button" class="btn btn-default editable-cancel"><i class="glyphicon glyphicon-remove"></i></button>',
            _tools = _table.find(".tooling");

        var replaceData = function () {
            var reObj = {};
            var showObj={};
            tarNode.find('td').find('input[type="text"]').each(function (i, td) {
                showObj[td.id]=reObj[td.id]=$(this).val();
            });

            tarNode.find('td').find('select').each(function (i, td) {
                reObj[td.id]=$(this).val();
                showObj[td.id]=$(this).find("option:selected").text();
            });
            _table.bootstrapTable('updateRow', {
                index: table.$data.thId,
                row: reObj
            });
            _tools.remove();
            table.editing = true;
            _table.trigger("edit.row.bs.table",[rowData[_table.data("idField")],reObj]);

            return false;
        };

        var recoveryData = function () {
            _table.bootstrapTable('updateRow', {
                index: table.$data.thId,
                row: {}
            });
            _tools.remove();
            table.editing = true;
            return false;
        };

        if (table.editing) {
            var rootid = 0;
            table.editing = false;
            table.columns.forEach(function (column, i) {
                if (!column.editable) return;
                var dom="";
                switch (column.editable) {
                    case 'input':
                        dom=$('<input type="text" class="form-control input-sm" id="' + column.field + '"/>').val(rowData[column.field]);
                        break;
                    case 'select':
                        dom = $('<select id="' + column.field + '" class="form-control input-sm">');
                        $.selectArray[column.field].forEach(function(item,i){
                            var data={"value":item.key};
                            if(item.key==rowData[column.field]){
                                data["selected"]="selected";
                            }
                            dom.append($("<option/>").attr(data).text(item.value));
                        });
                        break;
                    case 'textarea':
                        dom=$('<textarea class="form-control input-sm" id="' + column.field + '"/>').val(rowData[column.field]);
                        break;
                    default:
                        console.log(column.fieldIndex + ' ' + column.editable);
                }
                if(dom) {
                    tarNode.find('td').eq(column.fieldIndex).text('').append(dom);
                }

            }, evt);
            for (var i = 0, l = txt.length; i < l; i++) {
                tarNode.find('input[type="text"]').eq(i).val(txt[i]);
            }
            var _tooling = $('<div class="editable-buttons tooling btn-group pull-right btn-group-sm""/>');
            $(submit).on('click', replaceData).appendTo(_tooling);
            $(cancel).on('click', recoveryData).appendTo(_tooling);
            tarNode.find('td').last().append(_tooling);
        }
    };

    var BootstrapTable = $.fn.bootstrapTable.Constructor,
        _initTable = BootstrapTable.prototype.initTable,
        _initBody = BootstrapTable.prototype.initBody;

    BootstrapTable.prototype.initTable = function () {
        var that = this;
        this.$data = {};
        _initTable.apply(this, Array.prototype.slice.apply(arguments));
    };

    BootstrapTable.prototype.initBody = function () {
        var that = this;
        _initBody.apply(this, Array.prototype.slice.apply(arguments));

        if (!this.options.clickEdit) {
            return;
        }

        var table = this.$tableBody.find('table');
        that.editing = true;

        table.on('click-row.bs.table', function (e, row, $element, field) {
            this.$data.thId = $element.data().index;
            clikcToEdit(this, $element,row);
        }.bind(this));
    };
})(jQuery);