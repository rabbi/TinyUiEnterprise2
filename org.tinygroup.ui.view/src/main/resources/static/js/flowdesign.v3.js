(function ($) {
    var statusArr = {
        '0': 'default',
        '1': 'primary',
        '2': 'warning',
        '3': 'success',
        '4': 'danger',
        '5': 'inverse',
        '6': 'info'
    };
    var defaults = {
        processData: {},//步骤节点数据
        processUrl: '',//步骤节点数据
        fnRepeat: function () {

        },
        fnClick: function () {

        },
        fnDbClick: function () {

        },
        canvasMenus: {
            'one': function (t) {
                //alert('画面右键');
            }
        },
        processMenus: {
            'one': function (t) {
                //alert('步骤右键');
            }
        },
        /*右键菜单样式*/
        menuStyle: {
            border: '1px solid #5a6377',
            minWidth: '150px',
            padding: '5px 0'
        },
        itemStyle: {
            fontFamily: 'verdana',
            color: '#333',
            border: '0',
            /*borderLeft:'5px solid #fff',*/
            padding: '5px 40px 5px 20px'
        },
        itemHoverStyle: {
            border: '0',
            borderLeft: '5px solid #49afcd',
            color: '#fff',
            backgroundColor: '#5a6377'
        },
        mtAfterDrop: function (params) {
            //alert('连接成功后调用');
            //alert("连接："+params.sourceId +" -> "+ params.targetId);
        },
        //这是连接线路的绘画样式
        connectorPaintStyle: {
            lineWidth: 2,
            strokeStyle: '#49afcd',
            joinstyle: 'round'
        },
        connectorGrayStyle: {
            lineWidth: 2,
            strokeStyle: '#ccc',
            joinstyle: 'round'
        },
        //鼠标经过样式
        connectorHoverStyle: {
            lineWidth: 2,
            strokeStyle: '#da4f49'
        },
        readOnly: false
    };
    /*defaults end*/

    function getFSid(conn) {
        var fromId = conn.sourceId;
        fromId = fromId.substr(fromId.lastIndexOf('-') + 1);
        var targetId = conn.targetId;
        targetId = targetId.substr(targetId.lastIndexOf('-') + 1);
        return {from: parseInt(fromId), to: parseInt(targetId)};
    }

    $.fn.flowDesign = function (options) {
        var _canvas = $('<div>').addClass('tiny-flow-wrap').css({'position': 'relative'});

        $(this).append(_canvas);
        var activeId;
        var tiny_copy_id;
        //右键步骤的步骤号

        var _process_info;

        var option = $.extend({}, defaults, options);

        /*画布右键绑定*/
        var contextmenu = {
            bindings: option.canvasMenus,
            menuStyle: option.menuStyle,
            itemStyle: option.itemStyle,
            itemHoverStyle: option.itemHoverStyle
        };
        if (option.readOnly === false) {
            $(this).contextMenu('canvasMenu', contextmenu);
        }

        jsPlumb.importDefaults({
            DragOptions: {cursor: 'pointer'},
            Endpoint: ['Blank'],
            ConnectionOverlays: [
                ['Arrow', {
                    location: 1,
                    width: 8, length: 8
                }]
            ],
            Anchor: 'Continuous',//'AutoDefault',//
            ConnectorZIndex: 5,
            HoverPaintStyle: option.connectorHoverStyle
        });
        if ($.browser.msie && $.browser.version < '9.0') { //ie9以下，用VML画图
            jsPlumb.setRenderMode(jsPlumb.VML);
        } else { //其他浏览器用SVG
            jsPlumb.setRenderMode(jsPlumb.SVG);
        }

        var _canvas_design = function (lineData) {
            if (lineData) {
                $.each(lineData, function (i, row) {
                    if ($('#process-step-' + row.to).length > 0 && $('#process-step-' + row.from).length > 0) {
                        jsPlumb.connect({
                            source: 'process-step-' + row.from,
                            target: 'process-step-' + row.to
                        });
                    }
                });//each
            }
        };//_canvas_design end reset

        //连接线
        var initEndPoints = function () {
            _canvas.find('.process-flag').each(function (i, e) {
                var p = $(e).parent();
                var lineStyle = option.connectorPaintStyle;
                if (option.readOnly !== false && p.data('status') === 0) {
                    lineStyle = option.connectorGrayStyle;
                }
                jsPlumb.makeSource($(e), {
                    parent: p,
                    connector: ['Flowchart', {cornerRadius: 3, stub: [3, 3]}],
                    connectorStyle: lineStyle,
                    maxConnections: -1
                });
            });
        };

        var makeTarget = function () {
            _canvas.find('.process-step').each(function (i, e) {
                jsPlumb.makeTarget($(e), {
                    maxConnections: 1,
                    beforeDrop: function (params) {
                        var thisConn = getFSid(params);
                        var isBreak = false;
                        $.each(option.processData.process, function (i, row) {
                            if (row.index === thisConn.to && row.name === 'start') {
                                isBreak = true;
                                return false;
                            }
                        });
                        if (isBreak) {
                            return false;
                        }
                        $.each(option.processData.line, function (i, row) {
                            if (row.from === thisConn.to && row.to === thisConn.from) {
                                isBreak = true;
                                return false;
                            }
                        });
                        return !isBreak;
                    }
                });
            });
        };
        // 步骤
        var makeProcess = function (row) {
            var _nodeDiv = $('<div>');//document.createElement('div');
            var nodeId = 'process-step-' + row.index;
            var flagNode, titleNode;
            titleNode = $('<span>');
            var maxWidth = 400;
            var maxHeight = 400;
            flagNode = $('<span>').addClass('process-flag');
            if (row.icon) {
                titleNode.append($('<span>').addClass('badge').append($('<i>').addClass('fa ' + row.icon)).append(' '));
            }
            if (!row.status) {
                row.status = 0;
            }
            titleNode.append(row.title);
            _nodeDiv
                .attr({
                    id: nodeId,
                    style: row.view_info,
                    process_id: row.id,
                    'data-index': row.index,
                    'data-status': row.status
                })
                .css({
                    'position': 'absolute',
                    'min-width': '120px'
                })
                .addClass('process-step btn').addClass('btn-' + statusArr[row.status]) // 节点状态
                .append(titleNode).append(flagNode)
                .mousedown(function (e) {
                    if (row.name === 'start') {
                        return false;
                    }
                    _canvas.activeId = row.index;
                    if (e.which === 3) { //右键绑定
                        contextmenu.bindings = option.processMenus;
                        $(this).contextMenu('processMenu', contextmenu);
                    }
                });
            _canvas.append(_nodeDiv);
            if (option.readOnly === false) {
                jsPlumb.draggable(_nodeDiv);
            } else {
                var currentWidth = parseInt(_nodeDiv.css('left')) + parseInt(_nodeDiv.css('width'));
                var currentHeight = parseInt(_nodeDiv.css('top')) + parseInt(_nodeDiv.css('height'));
                if (maxWidth < currentWidth) {
                    maxWidth = currentWidth;
                }
                if (maxHeight < currentHeight) {
                    maxHeight = currentHeight;
                }
                _canvas.css({width: maxWidth + 'px', height: maxHeight + 'px'});
            }
        };


        var aConnections = [];
        //初始化原步骤
        var init = function (data) {
            jsPlumb.unbind('jsPlumbConnectionDetached');
            jsPlumb.unbind('jsPlumbConnection');
            jsPlumb.reset();
            _canvas.empty();
            _canvas.append('<div class="tiny_process_info"></div>');
            _process_info = _canvas.find('.tiny_process_info:first');
            if (data && data.process) {
                option.processData = data;
            }
            var processData = option.processData;

            if (!processData || !$.isArray(processData.process) || processData.process.length == 0) {
                processData = {
                    process: [{
                        id: '61',
                        index: 1,
                        title: '开 始',
                        name: 'start',
                        view_info: 'left:100px;top:30px;'
                    }], line: []
                };
            }

            $.each(processData.process, function (i, row) {
                makeProcess(row);
            });

            initEndPoints();
            makeTarget();
            _canvas_design(processData.line);

            if (option.readOnly === false) {
                jsPlumb.bind('jsPlumbConnection', function (info) {
                    setConnections(info.connection);
                });
                jsPlumb.bind('jsPlumbConnectionDetached', function (info) {
                    setConnections(info.connection, true);
                });
                jsPlumb.bind('click', function (c) {
                    var index=layer.confirm('你确定取消连接吗?', function () {
                        layer.close(index);
                        jsPlumb.detach(c);
                    });
                });
            }
        };
        if (option.processUrl) {
            $.ajax({
                url: option.processUrl,
                dataType: 'json',
                success: function (data) {
                    init(data);
                }
            });
        } else {
            init();
        }

        var timeout = null;
        //点击或双击事件,这里进行了一个单击事件延迟，因为同时绑定了双击事件
        _canvas.on('click', '.process-step', function () {
            //_canvas.activeId = $(this).data('index');
            clearTimeout(timeout);
            timeout = setTimeout(option.fnClick, 300);
        }).on('dblclick', '.process-step', function () {
            clearTimeout(timeout);
            option.fnDbClick();
        });


        var setConnections = function (conn, remove) {
            var lineArr = option.processData.line;
            var thisConn = getFSid(conn);
            if (!remove) lineArr.push(thisConn);
            else {
                for (var i = 0; i < lineArr.length; i++) {
                    if (lineArr[i].from === thisConn.from && lineArr[i].to === thisConn.to) {
                        lineArr.splice(i, 1);
                        break;
                    }
                }
            }
            jsPlumb.repaintEverything();//重画
        };

        var clear = function () {
            try {
                jsPlumb.detachEveryConnection();
                jsPlumb.deleteEveryEndpoint();
                _process_info.html('');
                jsPlumb.repaintEverything();
                return true;
            } catch (e) {
                return false;
            }
        };
//-----外部调用----------------------

        return {
            addProcess: function (row) {
                if (row.index <= 0) {
                    return false;
                }
                option.processData.process.push(row);
                makeProcess(row);
                initEndPoints();
                makeTarget();
                return true;
            },
            delProcess: function (activeId) {
                if (activeId <= 0) return false;
                var processData = option.processData;
                if (processData.line) {
                    $.each(processData.line, function (i, row) {
                        if (row) {
                            if (row.from == activeId || row.to == activeId) {
                                processData.line.splice(i, 1);
                            }
                        }
                    });
                }
                $.each(processData.process, function (i, row) {
                    if (row.index == activeId) {
                        processData.process.splice(i, 1);
                        return false;
                    }
                });
                init(processData);
                return true;
            },
            getActiveId: function () {
                return _canvas.activeId;
            },
            getProcessInfo: function () {
                var domArr = _canvas.find('div.process-step');
                $.each(option.processData.process, function (i, row) {
                    row.view_info = domArr.eq(i).attr('style');
                });
                return JSON.stringify(option.processData);
            },
            clear: function () {
                clear();
            },
            refresh: function (data) {
                if (option.processUrl) {
                    $.ajax({
                        url: option.processUrl,
                        cache: false,
                        dataType: 'json',
                        success: function (resData) {
                            init(resData);
                        }
                    });
                } else {
                    init(data);
                }
            }
        };
    };//
})(jQuery);